import React, {PureComponent} from 'react';
import './Main.css';
import data from './Main.data';

class Main extends PureComponent {
    static propTypes = {};

    static defaultProps = {};

    render() {
        const { phones } = data;

        return (
            <div className="Main">
                <header className="Main-header">
                    <img src={'/favicon.png'} className="Main-logo" alt="logo" />
                    <p>
                        Чистые ключи-2
                    </p>
                    <div className="Main-phones">
                        {phones.map(({name, phone}) => (
                            <div
                                key={phone}
                                className="Main-phone-row"
                            >
                                <div className="Main-phone-name">
                                    {name}
                                </div>
                                <a
                                    className="Main-phone-link"
                                    href={`tel:${phone}`}
                                >
                                    {phone}
                                </a>
                            </div>
                        ))}
                    </div>
                </header>
            </div>
        );
    }
}

export default Main;
