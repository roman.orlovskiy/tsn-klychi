import React, {PureComponent} from 'react';
import Main from 'pages/Main/Main';

class Root extends PureComponent {
  static propTypes = {};

  static defaultProps = {};

  render() {
    return (
      <div>
        <Main />
      </div>
    );
  }
}

export default Root;
