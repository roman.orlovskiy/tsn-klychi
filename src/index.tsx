import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Root from 'templates/Root';
import * as serviceWorker from './serviceWorker';

const rootElement = document.getElementById('root');
ReactDOM.render(<Root />, rootElement);

if ((module as any).hot) {
    (module as any).hot.accept('templates/Root', () => {
        const NextApp = require('templates/Root').default;
        ReactDOM.render(<NextApp />, rootElement);
    });
}

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
